package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import org.jboss.logging.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver,Integer> {
    @Modifying
    @Query(value = "UPDATE Caregiver c SET C.name = ?  WHERE c.id=?",nativeQuery = true)
    Integer updateC(String name,Integer id );
}
