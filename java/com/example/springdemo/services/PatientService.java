package com.example.springdemo.services;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PatientViewBuilder;
import com.example.springdemo.dto.builders.PersonViewBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientViewDTO findUserById(Integer id){
        Optional<Patient> patient = patientRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","user id",id);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }
    public List<PatientViewDTO> findAll(){
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
    public Integer insert(Patient patient){

        return patientRepository.save(patient).getId();
    }
    public Integer update(PatientDTO patientDTO){
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","user id",patientDTO.getId().toString());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }
    public void delete(PatientViewDTO patientViewDTO){
        this.patientRepository.deleteById(patientViewDTO.getId());
    }
}
