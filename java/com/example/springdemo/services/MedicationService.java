package com.example.springdemo.services;

import com.example.springdemo.dto.*;
import com.example.springdemo.dto.builders.*;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.Person;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }
    public Integer insert(MedicationDTO patientDTO){
        //PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(patientDTO)).getId();
    }
    public MedicationViewDTO findUserById(Integer id){
        Optional<Medication> patient = medicationRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","user id",id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(patient.get());
    }
    public List<MedicationViewDTO> findAll(){
        List<Medication> patients = medicationRepository.findAll();
        return patients.stream()
                .map(MedicationViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }





}
