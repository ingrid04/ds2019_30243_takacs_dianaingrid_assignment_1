package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.CaregiverViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public void delete(CaregiverViewDTO caregiverViewDTO){this.caregiverRepository.deleteById(caregiverViewDTO.getId());}
    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream().map(CaregiverViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }
    public CaregiverViewDTO findUserById(Integer id){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if(caregiver.isPresent()==false){
            throw new ResourceNotFoundException("Caregiver","user id",id);
        }
        return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }
    public Integer insert(CaregiverDTO caregiverDTO){
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }
    public Integer update(CaregiverDTO caregiverDTO){
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());
        if(caregiver.isPresent()==false){
            throw new ResourceNotFoundException("Caregiver","user id",caregiverDTO.getId().toString());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();

    }
}
