package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.dto.builders.DoctorViewBuilder;
import com.example.springdemo.dto.builders.PersonBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import com.example.springdemo.validators.DoctorFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findUserById(Integer id){
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor","user id",id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.findAll();
        return doctors.stream().map(DoctorViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }
    public Integer insert(DoctorDTO doctorDTO){
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);
        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }
    public Integer update(DoctorDTO doctorDTO){
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor","user id",doctorDTO.getId().toString());
        }
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }
    public void delete(DoctorViewDTO doctorViewDTO){
        this.doctorRepository.deleteById(doctorViewDTO.getId());
    }
}
