package com.example.springdemo.validators;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class DoctorFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(DoctorFieldValidator.class);
    private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    public static void validateInsertOrUpdate(DoctorDTO doctorDTO) {

        List<String> errors = new ArrayList<>();
        if (doctorDTO == null) {
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DoctorFieldValidator.class.getSimpleName(), errors);
        }
    }
}
