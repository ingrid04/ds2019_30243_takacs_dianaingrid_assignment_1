package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="medicationList")
public class MedicationList {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    /*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicationPlan_id")
    private MedicationPlan medicationPlan;





    @OneToMany(mappedBy = "medicationList",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Medication> medicationArray = new ArrayList<Medication>();



     */
    public MedicationList(Integer id,String name) {
        this.id=id;
        this.name = name;
    }
    public MedicationList(String name) {
        this.name = name;
    }

    public MedicationList() {
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationList that = (MedicationList) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
