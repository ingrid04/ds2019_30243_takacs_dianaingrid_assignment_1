package com.example.springdemo.entities;

import javax.persistence.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;
/*
DONE
 */
@Entity(name="Caregiver")
@Table(name="caregiver")
public class Caregiver {
    //DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name="birthDate")
    private LocalDate birthDate;

    @Column(name="gender",length = 1)
    private char gender;

    @Column(name = "address",length = 100)
    private String address;

    @OneToMany(
            mappedBy = "caregiver",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Patient> patientList = new ArrayList<Patient>();



    public Caregiver(){}

    public Caregiver(String name, LocalDate birthDate, char gender, String address, List<Patient> patientList) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patientList = patientList;
    }
    public Caregiver(String name, LocalDate birthDate, char gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }
    public Caregiver(Integer id,String name, LocalDate birthDate, char gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.id=id;
    }

    public void addPatient(Patient patient) {
        patientList.add(patient);
        patient.setCaregiver(this);
    }

    public void removePatient(Patient patient) {
        patientList.remove(patient);
        patient.setCaregiver(null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Caregiver caregiver = (Caregiver) o;
        return gender == caregiver.gender &&
                Objects.equals(id, caregiver.id) &&
                Objects.equals(name, caregiver.name) &&
                Objects.equals(birthDate, caregiver.birthDate) &&
                Objects.equals(address, caregiver.address) &&
                Objects.equals(patientList, caregiver.patientList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address, patientList);
    }
}
