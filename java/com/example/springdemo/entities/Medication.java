package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;
@Entity(name="Medication")
@Table(name="medication")
public class Medication {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medication_id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name="dosaje")
    private Integer dosaje;

    @Column(name="hourToGet")
    private int interval;

    /*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicationList_id")
    private MedicationList medicationList;


     */
    @OneToMany(mappedBy = "medication",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<SideEffect> sideEffectsList = new ArrayList<SideEffect>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "medications")
    private Set<MedicationPlan>medicationPlanSet=new HashSet<>();

    public Medication(String name, Integer dosaje, int interval) {
        this.name = name;
        this.dosaje = dosaje;
        this.interval = interval;
    }
    public Medication(Integer id,String name, Integer dosaje, int interval) {
        this.name = name;
        this.dosaje = dosaje;
        this.interval = interval;
    }

    public Medication() {
    }

    public Medication(String name, Integer dosaje) {
        this.name = name;
        this.dosaje = dosaje;
    }

    public void addSideEffect(SideEffect sideEffect){

        sideEffectsList.add(sideEffect);
        sideEffect.setMedication(this);
    }

    public void removeSideEffect(SideEffect sideEffect){
        sideEffectsList.remove(sideEffect);
        sideEffect.setMedication(null);
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Integer getDosaje() {
        return dosaje;
    }

    public void setDosaje(Integer dosaje) {
        this.dosaje = dosaje;
    }


    public List<SideEffect> getSideEffectsList() {
        return sideEffectsList;
    }

    public void setSideEffectsList(List<SideEffect> sideEffectsList) {
        this.sideEffectsList = sideEffectsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(dosaje, that.dosaje) &&
                Objects.equals(sideEffectsList, that.sideEffectsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, dosaje, sideEffectsList);
    }
}
