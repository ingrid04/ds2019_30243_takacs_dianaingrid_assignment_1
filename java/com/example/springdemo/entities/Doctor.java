package com.example.springdemo.entities;

import jdk.nashorn.internal.objects.annotations.Getter;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

 /*
 DONE for medPlan
  */
@Entity(name="Doctor")
@Table(name = "doctor")

public class Doctor {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @OneToMany(
            mappedBy = "doctor",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MedicationPlan> medicationPlanList = new ArrayList<MedicationPlan>();



    public void addMedicationPlan(MedicationPlan medicationPlan)
    {
        medicationPlanList.add(medicationPlan);
        medicationPlan.setDoctor(this);
    }

    public void removeMedicationPlan(MedicationPlan medicationPlan)
    {
        medicationPlanList.remove(medicationPlan);
        medicationPlan.setDoctor(null);
    }



    public Doctor(){}
    public Doctor(Integer id, String name) {
        this.id=id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     @Override
     public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Doctor doctor = (Doctor) o;
         return Objects.equals(id, doctor.id) &&
                 Objects.equals(name, doctor.name) &&
                 Objects.equals(medicationPlanList, doctor.medicationPlanList);
     }

     @Override
     public int hashCode() {
         return Objects.hash(id, name, medicationPlanList);
     }

     public List<MedicationPlan> getMedicationPlanList() {
         return medicationPlanList;
     }

     public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
         this.medicationPlanList = medicationPlanList;
     }
 }
