package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity(name="MedicationPlan")
@Table(name="medicationPlan")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medPlan_id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;


    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name = "medPlan_med", joinColumns = {
            @JoinColumn(name = "medPlan_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "medication_id",
                    nullable = false, updatable = false) })
    private Set<Medication> medications = new HashSet<>();


    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Patient patient;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Period period;

   /* @OneToMany(
            mappedBy = "medicationPlan",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MedicationList> medicationListList = new ArrayList<MedicationList>();





    public void addMedicationList(MedicationList medicationList){
        medicationListList.add(medicationList);
        medicationList.setMedicationPlan(this);
    }

    public void removeMedicationList(MedicationList medicationList){
        medicationListList.remove(medicationList);
        medicationList.setMedicationPlan(null);
    }


    */


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public MedicationPlan(Integer id){this.id=id;}
    public MedicationPlan(){}
    public MedicationPlan(Doctor doctor, Patient patient, Period period) {
        this.doctor = doctor;
        this.patient = patient;
        this.period = period;
    }
    public MedicationPlan(Integer id,Doctor doctor, Patient patient, Period period) {
        this.doctor = doctor;
        this.patient = patient;
        this.period = period;
        this.id=id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlan that = (MedicationPlan) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(period, that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, doctor, patient, period);
    }
}
