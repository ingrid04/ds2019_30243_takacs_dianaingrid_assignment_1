package com.example.springdemo.entities;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;
/*
DONE
 */
@Entity
@Table(name="patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name="birthDate")
    private LocalDate birthDate;

    @Column(name="gender",length = 1)
    private char gender;

    @Column(name = "address",length = 100)
    private String address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;



    public Patient() {
    }
    public Patient(String name, LocalDate birthDate, char gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }
    public Patient(String name, LocalDate birthDate, char gender, String address, Caregiver caregiver) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.caregiver = caregiver;
    }
    public Patient(Integer id,String name, LocalDate birthDate, char gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.id=id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return gender == patient.gender &&
                Objects.equals(id, patient.id) &&
                Objects.equals(name, patient.name) &&
                Objects.equals(birthDate, patient.birthDate) &&
                Objects.equals(address, patient.address) &&
                Objects.equals(caregiver, patient.caregiver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address, caregiver);
    }
}
