package com.example.springdemo.dto;

import java.util.Objects;

public class MedicationDTO {
    private Integer id;


    private String name;


    private Integer dosaje;


    private int interval;

    public MedicationDTO(){}
    public MedicationDTO(Integer id, String name, Integer dosaje, int interval) {
        this.id = id;
        this.name = name;
        this.dosaje = dosaje;
        this.interval = interval;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDosaje() {
        return dosaje;
    }

    public void setDosaje(Integer dosaje) {
        this.dosaje = dosaje;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO that = (MedicationDTO) o;
        return interval == that.interval &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(dosaje, that.dosaje);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, dosaje, interval);
    }
}
