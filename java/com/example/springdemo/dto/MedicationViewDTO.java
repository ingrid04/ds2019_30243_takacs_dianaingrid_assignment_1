package com.example.springdemo.dto;

public class MedicationViewDTO {
    private Integer id;


    private String name;


    private Integer dosaje;


    private int interval;

    public MedicationViewDTO(Integer id, String name, Integer dosaje, int interval) {
        this.id = id;
        this.name = name;
        this.dosaje = dosaje;
        this.interval = interval;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDosaje() {
        return dosaje;
    }

    public void setDosaje(Integer dosaje) {
        this.dosaje = dosaje;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
