package com.example.springdemo.dto;

import javax.persistence.Column;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class CaregiverDTO {
    private Integer id;
    private String name;
    private LocalDate birthDate;
    private char gender;
    private String address;
    private List<PatientDTO>patientDTOList;

    public CaregiverDTO(){}

    public CaregiverDTO(Integer id, String name, LocalDate birthDate, char gender, String address,List<PatientDTO>patientDTOList) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patientDTOList=patientDTOList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientDTO> getPatientDTOList() {
        return patientDTOList;
    }

    public void setPatientDTOList(List<PatientDTO> patientDTOList) {
        this.patientDTOList = patientDTOList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return gender == that.gender &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address);
    }
}
