package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.dto.PersonViewDTO;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Person;

public class DoctorViewBuilder {
    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(doctor.getId(),doctor.getName());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(doctorViewDTO.getId(),doctorViewDTO.getName());
    }
}
