package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationViewDTO;
import com.example.springdemo.entities.Medication;

public class MedicationViewBuilder {
    public static MedicationViewDTO generateDTOFromEntity(Medication medication) {
        return new MedicationViewDTO(medication.getId(),medication.getName(),medication.getDosaje(),medication.getInterval());
    }

    public static Medication generateDTOFromEntity(MedicationViewDTO medicationViewDTO){
        return new Medication(medicationViewDTO.getId(),medicationViewDTO.getName(),medicationViewDTO.getDosaje(),medicationViewDTO.getInterval());

    }
}
