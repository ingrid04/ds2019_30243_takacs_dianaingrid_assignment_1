package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {
    public CaregiverBuilder(){}
    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(caregiver.getId(),caregiver.getName(),caregiver.getBirthDate(),caregiver.getGender(),caregiver.getAddress(),null);
    }
    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(caregiverDTO.getId(),caregiverDTO.getName(),caregiverDTO.getBirthDate(),caregiverDTO.getGender(),caregiverDTO.getAddress());
    }
}
