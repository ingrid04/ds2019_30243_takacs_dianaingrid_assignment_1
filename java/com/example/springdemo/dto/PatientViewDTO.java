package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;

import java.time.LocalDate;
import java.util.Calendar;

public class PatientViewDTO {
    private Integer id;
    private String name;
    private LocalDate birthDate;
    private char gender;
    private String address;
    private Caregiver caregiverViewDTO;

    public PatientViewDTO(Integer id, String name, LocalDate birthDate, char gender, String address, Caregiver caregiverViewDTO) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.caregiverViewDTO = caregiverViewDTO;
    }

    public PatientViewDTO(Integer id, String name, LocalDate birthDate, char gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Caregiver getCaregiverViewDTO() {
        return caregiverViewDTO;
    }

    public void setCaregiverViewDTO(Caregiver caregiverViewDTO) {
        this.caregiverViewDTO = caregiverViewDTO;
    }
}
