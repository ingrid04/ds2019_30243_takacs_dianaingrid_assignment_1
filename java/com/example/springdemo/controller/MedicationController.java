package com.example.springdemo.controller;


import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.MedicationViewDTO;
import com.example.springdemo.services.MedicationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/medication")
public class MedicationController {
    private final MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping("/{id}")
    public MedicationViewDTO findById(@PathVariable Integer id){
        return medicationService.findUserById(id);
    }
    @GetMapping("/findAllMedications")
    public List<MedicationViewDTO> findAllM(){
        return medicationService.findAll();
    }
    @PutMapping("insertMedication/{id}/{name}")
    public int insertMedication(@PathVariable("id")Integer id, @PathVariable("name") String name){
        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setName("Trachisan");
        medicationDTO.setId(9);
        medicationDTO.setInterval(10);
        medicationDTO.setDosaje(16);
        return medicationService.insert(medicationDTO);
    }
}
