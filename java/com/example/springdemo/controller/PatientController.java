package com.example.springdemo.controller;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.PersonViewDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PatientViewBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.PatientService;
import com.example.springdemo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    private final PatientService patientService;
    Patient patientDTO = new Patient(5,"Mirceal", LocalDate.now(),'M',"Alba");

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService=patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findUserById(id);
    }



    @GetMapping("/findAllPatients")
    public List<PatientViewDTO> findAllP(){
        return patientService.findAll();}

    @PostMapping("/insertPatient")
    public Integer insertPatientDTO()//@PathVariable String name, @PathVariable Integer idC){
    {//PatientDTO patientDTO = new PatientDTO();
        //patientDTO.setName(name);
        return patientService.insert(patientDTO);
    }

    @PutMapping("/updatePatient/{id}/{name}/{gender}")
    public Integer updatePatient(@PathVariable Integer id,@PathVariable String name,@PathVariable char gender) {
        PatientViewDTO patientViewDTO=patientService.findUserById(id);
        patientViewDTO.setName(name);
        patientViewDTO.setGender(gender);
        Patient patient = PatientViewBuilder.generateEntityFromDTO(patientViewDTO);
        PatientDTO patientDTO = PatientBuilder.generateDTOFromEntity(patient);
        return patientService.update(patientDTO);
    }

    @DeleteMapping("/deletePatient/{id}/{name}")
    public void delete(@PathVariable Integer id,@PathVariable Double name){
        PatientViewDTO patientViewDTO = patientService.findUserById(id);
        patientService.delete(patientViewDTO);
    }

}
