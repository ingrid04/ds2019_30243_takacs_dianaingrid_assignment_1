package com.example.springdemo.controller;

import com.example.springdemo.dto.*;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.dto.builders.DoctorViewBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PatientViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.CaregiverService;
import com.example.springdemo.services.DoctorService;
import com.example.springdemo.services.MedicationService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value="/doctor")
public class DoctorController {
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final CaregiverService caregiverService;
    private final MedicationService medicationService;

    PatientDTO patientDTO = new PatientDTO(5,"Mirceal", LocalDate.now(),'M',"Alba");

    @Autowired
    public DoctorController(MedicationService medicationService,DoctorService doctorService, PatientService patientService, CaregiverService caregiverService){
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.caregiverService = caregiverService;
        this.medicationService=medicationService;
    }

    //doctor
    @GetMapping(value="/{id}")
    public DoctorViewDTO findByDId(@PathVariable("id") Integer id){return doctorService.findUserById(id);}

    @GetMapping("/findAllDoctors")
    public List<DoctorViewDTO> findAll(){return doctorService.findAll();}

    @PostMapping("/insert-doctor/{name}")
    public Integer insertUserDTO(@PathVariable String name){
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setName(name);
        return doctorService.insert(doctorDTO);}

    @PutMapping("/update-doctor")
    public Integer updatrUser(@RequestBody DoctorDTO doctorDTO){return doctorService.update(doctorDTO);}

    @DeleteMapping("/delete-doctor/{idD}")
    public void delete(@PathVariable Integer idD){
        DoctorViewDTO doctorViewDTO = doctorService.findUserById(idD);
        doctorService.delete(doctorViewDTO);}


}

