package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.builders.CaregiverViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.CaregiverService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value="/caregiver")
public class CaregiverController {
    private final CaregiverService caregiverService;

    private final PatientService patientService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService,PatientService patientService) {
        this.caregiverService = caregiverService;
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public List<PatientViewDTO> findById(@PathVariable("id") Integer id){
        CaregiverViewDTO caregiverViewDTO = caregiverService.findUserById(id);

        return patientService.findAll().stream().filter(p->p.getCaregiverViewDTO().equals(caregiverViewDTO)).collect(Collectors.toList());
        }

    @GetMapping(value = "/caregiver/{id}")
    public List<PatientViewDTO> findByCId(@PathVariable("id") Integer id){
        CaregiverViewDTO caregiverViewDTO = caregiverService.findUserById(id);

        return patientService.findAll().stream().filter(p->p.getCaregiverViewDTO().equals(caregiverViewDTO)).collect(Collectors.toList());
    }
    @GetMapping("/findAllCaregivers")
    public List<CaregiverViewDTO> findAllCaregivers(){        return caregiverService.findAll();}
    @PostMapping("/insertCaregiver/{name}/{gender}")
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO, @PathVariable String name, @PathVariable char gender){
        caregiverDTO = new CaregiverDTO();
        caregiverDTO.setName(name);
        caregiverDTO.setGender(gender);
        return caregiverService.insert(caregiverDTO);}
    @PutMapping("/updateCaregiver")
    public Integer updateUser(@RequestBody CaregiverDTO caregiverDTO){return caregiverService.update(caregiverDTO);}
    @DeleteMapping("/deleteCaregiver/{idC}")
    public void delete(@PathVariable int idC){
        CaregiverViewDTO caregiverViewDTO = caregiverService.findUserById(idC);
        caregiverService.delete(caregiverViewDTO);}



}


